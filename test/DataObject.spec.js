import { builddataobject } from '../src';

const jsData = {
    lecturers: {},
    tags: {},
    sites: {},
    groups: {},
    attendees: {},
};

const calEvent = {
    tutors: {},
    sites: {},
    tags: [],
    jsData,
};


describe('Data object', () => {
    describe('start', () => {
        it('represents the startimg time of an event in seconds', () => {
            const dummyTimeStamp = 1234567890;

            const { start } = builddataobject(calEvent, jsData, dummyTimeStamp);

            expect(start).toBe(dummyTimeStamp / 1000);
        });
    });
    describe('end', () => {
        it('is 30 minutes after the start time', () => {
            const dummyTimeStamp = 1234567890;

            const {
                start,
                end,
            } = builddataobject(calEvent, jsData, dummyTimeStamp);

            expect(end).toBeGreaterThan(start);
            expect((end - start) / 60).toBe(30);
        });
    });
    describe('AllTutors', () => {
        it('is copied from jsData', () => {
            jsData.lecturers = {
                '123': 456,
            };

            const { AllTutors } = builddataobject(calEvent, jsData);

            expect(AllTutors).toMatchObject(jsData.lecturers);
        });
    });
    describe('AllTags', () => {
        it('is copied from jsData', () => {
            jsData.tags = {
                '123': 456,
            };

            const { AllTags } = builddataobject(calEvent, jsData);

            expect(AllTags).toMatchObject(jsData.tags);
        });
    });
    describe('AllSites', () => {
        it('is copied from jsData', () => {
            jsData.sites = {
                '123': 456,
            };

            const { AllSites } = builddataobject(calEvent, jsData);

            expect(AllSites).toMatchObject(jsData.sites);
        });
    });
    describe('AllGroups', () => {
        it('is copied from jsData', () => {
            jsData.groups = {
                '123': 456,
            };

            const { AllGroups } = builddataobject(calEvent, jsData);

            expect(AllGroups).toMatchObject(jsData.groups);
        });
    });
    describe('AllAttendees', () => {
        it('is copied from jsData', () => {
            jsData.attendees = { '123': 456  };

            const { AllAttendees } = builddataobject(calEvent, jsData);

            expect(AllAttendees).toMatchObject(jsData.attendees);
        });
    });
    describe('nested_locations', () => {
        it('is copied from jsData', () => {
            jsData.nested_locations = { '123': 456  };

            const { nested_locations } = builddataobject(calEvent, jsData);

            expect(nested_locations).toMatchObject(jsData.nested_locations);
        });
    });
});
