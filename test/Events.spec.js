import { buildgroupevents } from '../src/buildgroupevents';

const skeletonEvent = {
    isgroupteaching: 1,
    tutors: {},
    locations: {},
    attendees: {},
};

describe('Group teaching event', () => {
    describe('location', () => {
        describe('id', () => {
            it('matches the ID of the location assigned to the group', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    locations: {
                        '6297': {
                            id: 16,
                            building: 'Maxwell Dworkin',
                            room: 'Conference Room',
                            forgroup: 314,
                        },
                    },
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;
                expect(event.locationid).toBe(16);
            });
            it('defaults to TBC', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.locationid).toBe('TBC');
            });
        });
        describe('name', () => {
            it('includes the building name', () => {

                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    locations: {
                        '6297': {
                            id: 16,
                            building: 'Maxwell Dworkin',
                            room: 'Conference Room',
                            forgroup: 314,
                        },
                    },
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;
                expect(event.locationname).toMatch('Maxwell Dworkin');
            });
            it('includes the room name', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    locations: {
                        '6297': {
                            id: 16,
                            building: 'Maxwell Dworkin',
                            room: 'Conference Room',
                            forgroup: 314,
                        },
                    },
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;
                expect(event.locationname).toMatch('Conference Room');
            });
            it('defaults to TBC', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.locationid).toBe('TBC');
            });
        });
    });

    describe('tutor', () => {
        describe('id', () => {
            it('matches the ID of the tutor assigned to the group', () => {
                const [ event ]  = buildgroupevents({
                    ...skeletonEvent,
                    tutors: {
                        '142': {
                            id: 142,
                            title: 'Mr',
                            firstname: 'John',
                            lastname: 'Example',
                            forgroup: 314,
                        },
                    },
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;
                expect(event.tutorid).toBe(142);
            });
            it('is undefined if the group has no tutor assigned', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.tutorid).not.toBeDefined();
            });
        });
        describe('name', () => {
            it('is undefined if the group has no tutor assigned', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.tutorname).not.toBeDefined();
            });

            it('matches the name of the tutor assigned to the group', () => {
                const [ event ]  = buildgroupevents({
                    ...skeletonEvent,
                    tutors: {
                        '142': {
                            id: 142,
                            title: 'Mr',
                            firstname: 'John',
                            lastname: 'Example',
                            forgroup: 314,
                        },
                    },
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.tutorname).toMatch(/Mr/);
                expect(event.tutorname).toMatch(/John Example/);
            });
        });
    });

    describe('group', () => {
        describe('id', () => {
            it('id is the primary key ID of the group', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.groupid).toBe(8274);
            });
        });
        describe('name', () => {
            it('id is the name of the group attached to this event', () => {
                const [ event ] = buildgroupevents({
                    ...skeletonEvent,
                    attendees: {
                        '314': {
                            a_id: 314,
                            a_attendee_id: 8274,
                            a_name: 'Anatomy Group D2',
                        },
                    },
                }).groupteachingevents;

                expect(event.groupname).toBe('Anatomy Group D2');
            });
        });
    });
});
