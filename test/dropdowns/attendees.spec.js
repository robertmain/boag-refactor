import { buildsattendeesselectinnerhtml } from '../../src';

describe('Attendees dropdown', () => {

    it('pre-selects attendees assigned to an event', () => {
        const attendees = buildsattendeesselectinnerhtml({
            attendees: {
                '285': {
                    a_attendee_id: 285,
                },
            },
            AllGroups: {
                '9132': {
                    id: 9132,
                    name: 'Year 1',
                    acyr: 2012,
                },
            },
            AllAttendees: {
                '285': {
                    id: 285,
                    name: 'Mr J Smith',
                    email: 'j.smith@domain.com',
                },
            },
        });

        expect(attendees).toHaveLength(2);

        const [ group, user ] = attendees;

        expect(group.value).toBe('G|9132');
        expect(group.text).toBe('Group: Year 1 (2012)');
        expect(group.selected).toBe(false);

        expect(user.value).toBe('U|285');
        expect(user.text).toBe('Mr J Smith (j.smith@domain.com)');
        expect(user.selected).toBeDefined();
    });

    it('lists all attendees available for selection', () => {
        const attendees = buildsattendeesselectinnerhtml({
            attendees: {},
            AllGroups: {},
            AllAttendees: {
                '9910': {
                    id: 9910,
                    name: 'Mr P Sherman',
                    email: 'p.sherman@gmail.com',
                },
                '9275': {
                    id: 9275,
                    name: 'Mrs H Anderson',
                    email: 'h.anderson@gmail.com',
                },
                '4627': {
                    id: 4627,
                    name: 'Ms D Smith',
                    email: 'd.smith@gmail.com',
                },
            },
        });

        expect(attendees).toHaveLength(3);

        const [ sherman, anderson, smith ] = attendees;

        expect(sherman.text).toBe('Ms D Smith (d.smith@gmail.com)');
        expect(anderson.text).toBe('Mrs H Anderson (h.anderson@gmail.com)');
        expect(smith.text).toBe('Mr P Sherman (p.sherman@gmail.com)');
    });

    it('contains no duplicate entries', () => {
        const attendees = buildsattendeesselectinnerhtml({
            AllGroups: {
                '8912': {
                    id: 8912,
                    name: 'Test Group',
                    acyr: 2012,
                },
                // eslint-disable-next-line no-dupe-keys
                '8912': {
                    id: 8912,
                    name: 'Test Group',
                    acyr: 2012,
                },
            },
            AllAttendees: {
                '8912': {
                    id: 8912,
                    name: 'Ms P Anderson',
                    email: 'p.anderson@gmail.com',
                },
            },
        });

        expect(attendees).toHaveLength(2);

        const [group, user] = attendees;

        expect(group.value).toBe('G|8912');
        expect(group.text).toBe('Group: Test Group (2012)');

        expect(user.value).toBe('U|8912');
        expect(user.text).toBe('Ms P Anderson (p.anderson@gmail.com)');
    });
});
