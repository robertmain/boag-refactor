import { buildstutorselectinnerhtml } from '../../src';

describe('Tutors dropdown', () => {
    it('pre-selects tutors assigned to an event', () => {
        const [alan, helen] = buildstutorselectinnerhtml({
            tutors: {
                '2821': {
                    id: 2821,
                    title: 'Mr',
                    firstname: 'Alan',
                    lastname: 'Taylor',
                },
                '8294': {
                    id: 8294,
                    title: 'Mrs',
                    firstname: 'Helen',
                    lastname: 'Bell',
                },
            },
            AllTutors: {},
        });

        expect(alan.value).toBe('2821');
        expect(alan.selected).toBe(false);

        expect(helen.value).toBe('8294');
        expect(helen.selected).toBe(false);
    });

    it('lists all tutors available for selection', () => {
        const tutors = buildstutorselectinnerhtml({
            tutors: {
                '2691': {
                    id: 809,
                    title: 'Mr',
                    firstname: 'Adam',
                    lastname: 'Smith',
                    forgroup: 314,
                },
                '2719': {
                    id: 204,
                    title: 'Miss',
                    firstname: 'Helen',
                    lastname: 'Adams',
                    forgroup: 600,
                },
            },
            AllTutors: {},
        });

        const [ adam, helen ] = tutors;

        expect(tutors.length).toBe(2);
        expect(adam.text).toBe('Mr A Smith');
        expect(helen.text).toBe('Miss H Adams');
    });

    it('conatins no duplicate entries', () => {
        const tutors = buildstutorselectinnerhtml({
            tutors: {
                '2821': {
                    id: 2821,
                    title: 'Ms',
                    firstname: 'Jennifer',
                    lastname: 'Warren',
                },
            },
            AllTutors: {
                '9163': {
                    id: 2821,
                    name: 'Ms J Warren',
                },
            },
        });

        expect(tutors.length).toBe(1);
    });

    it('indicates inactive accounts', () => {
        const tutors = buildstutorselectinnerhtml({
            tutors: {},
            AllTutors: {
                '9024': {
                    id: 9024,
                    name: 'Mrs H Anderson',
                    deleted: true,
                },
            },
        });

        expect(tutors.length).toBe(1);
        expect(tutors[0].text).toContain('(Account Inactive)');
    });
});
