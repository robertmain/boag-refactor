import { buildstagselectinnerhtml } from '../../src';

describe('Tags dropdown', () => {
    it('pre-selects tags assigned to an event', () => {
        const tags = buildstagselectinnerhtml({
            tags: [
                {
                    tagid: 264,
                },
            ],
            AllTags: [
                {
                    id: 264,
                    name: 'Year 4',
                },
            ],
        });

        expect(tags).toHaveLength(1);

        const [ year4 ] = tags;

        expect(year4.value).toBe('264');
        expect(year4.text).toContain('Year 4');
        expect(year4.selected).toBeDefined();
    });

    it('lists all tags available for selection', () => {
        const tags = buildstagselectinnerhtml({
            tags: [],
            AllTags: [
                {
                    id: 628,
                    name: 'Medicine',
                },
                {
                    id: 291,
                    name: 'Dentistry',
                },
                {
                    id: 819,
                    name: 'Anatomy',
                },
            ],
        });

        expect(tags).toHaveLength(3);

        const [ medicine, dentistry, anatomy ] = tags;

        expect(medicine.text).toContain('Medicine');
        expect(medicine.value).toBe('628');
        expect(medicine.selected).toBe(false);

        expect(dentistry.text).toContain('Dentistry');
        expect(dentistry.value).toBe('291');
        expect(dentistry.selected).toBe(false);

        expect(anatomy.text).toContain('Anatomy');
        expect(anatomy.value).toBe('819');
        expect(anatomy.selected).toBe(false);
    });

    it('contains no duplicate entries', () => {
        const tags = buildstagselectinnerhtml({
            tags: [],
            AllTags: [
                {
                    id: 982,
                    name: 'Anatomy',
                },
                {
                    id: 264,
                    name: 'Year 4',
                },
                {
                    id: 264,
                    name: 'Year 4',
                },
            ],
        });

        expect(tags).toHaveLength(2);

        const [ anatomy, year4 ] = tags;

        expect(anatomy.value).toBe('982');
        expect(anatomy.text).toBe('Anatomy');

        expect(year4.value).toBe('264');
        expect(year4.text).toBe('Year 4');
    });
});
