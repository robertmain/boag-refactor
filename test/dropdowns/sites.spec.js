import { buildssitesselectinnerhtml } from '../../src';

describe('Sites dropdown', () => {
    it('pre-selects sites assigned to an event', () => {
        const sites = buildssitesselectinnerhtml({
            sites: [
                {
                    siteid: 264,
                },
            ],
            AllSites: [
                {
                    id: 264,
                    title: 'My Application',
                },
            ],
        });

        expect(sites).toHaveLength(1);

        const [ myApplication ] = sites;

        expect(myApplication.value).toBe('264');
        expect(myApplication.text).toContain('My Application');
        expect(myApplication.selected).toBeDefined();
    });

    it('lists all tags available for selection', () => {
        const sites = buildssitesselectinnerhtml({
            sites: [],
            AllSites: [
                {
                    id: 628,
                    title: 'Example Web App',
                },
                {
                    id: 291,
                    title: 'Schedule Manager',
                },
                {
                    id: 819,
                    title: 'Database Monitor',
                },
            ],
        });

        expect(sites).toHaveLength(3);

        const [ example, scheduleManager, dbMonitor ] = sites;

        expect(example.value).toBe('628');
        expect(scheduleManager.value).toBe('291');
        expect(dbMonitor.value).toBe('819');
    });

    it('contains no duplicate entries', () => {
        const sites = buildssitesselectinnerhtml({
            sites: [],
            AllSites: [
                {
                    id: 982,
                    title: 'My Web App',
                },
                {
                    id: 264,
                    title: 'Testing',
                },
                {
                    id: 264,
                    title: 'Testing',
                },
            ],
        });

        expect(sites).toHaveLength(2);

        const [ myWebApp, testing ] = sites;

        expect(myWebApp.value).toBe('982');
        expect(testing.value).toBe('264');
    });
});
