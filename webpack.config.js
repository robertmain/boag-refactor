const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { EOL } = require('os');
const {
    name,
    description,
    author,
    homepage,
    license,
} = require('./package.json');

module.exports = ({ mode = 'development' }) => ({
    mode,
    entry: {
        main: './src/index.js',
    },
    output: {
        filename: '[name].js',
        libraryTarget: 'window',
    },
    externals: {
        jquery: {
            commonjs: 'jquery',
            commonjs2: 'jquery',
            amd: 'jquery',
            root: '$',
        },
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            '@': path.join(__dirname, '..', 'src'),
        },
    },
    optimization: {
        splitChunks: {
            chunks: 'initial',
            name: 'vendor',
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new webpack.BannerPlugin({
            banner: () => (
                name[0].toUpperCase() + name.slice(1) + EOL +
                description + EOL +
                EOL +
                Object.entries({
                    copyright: `${author} ${new Date().getFullYear()}`,
                    version: '[hash]',
                    link: homepage,
                    license,
                }).map(([key, value]) => `@${key} ${value}`).join(EOL)
            ),
        }),
    ],
});
