import { dropdown } from './components';
import { FLAGS, STATUS } from './constants';
import { buildgroupevents } from './buildgroupevents';
import $ from 'jquery';

const wzData = {};   // global objects used to hold information
const gData = {};    // for times that i cant pass it or reach it

// Global variables for comparing label names and input ID's
const { ALL_DAY, PRS_REQUIRED, GROUP_TEACHING } = FLAGS;

// Form field value comparison global variables
const { PUBLISHED, CONFIRMED, CANCELLED } = STATUS;

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
export const hide_id = () => {
    $('td:nth-child(0)').hide();
    $('td:nth-child(0),th:nth-child(0)').hide();
};

/**
 * Merge the data from `jsData` into the provided `calEvent`. The resulting
 * object is then both set as a property of `window.gData` and returned
 *
 * @function builddataobject
 *
 * @param {Object} calEvent A calendar event
 * @param {Object} calEvent.jsData A copy of the data from `jsData`
 *
 * @param {Object} jsData
 * @param {Object[]} jsData.lecturers All lecturers available for selection against this event
 * @param {Object[]} jsData.tags  All tags available for selection against this event
 * @param {Object[]} jsData.sites All client applications available for selction against this event
 * @param {Object[]} jsData.groups All schedule groups eligible to be assigned to this event
 * @param {Object[]} jsData.attendees All students eligible to be assigned to this event
 * @param {Object} jsData.nested_locations All locations in the database. Keyed by building name against rooms in that building
 * @param {Number} [startdatetime]  The start time of the event as a unix timestamp in milliseconds
 *
 * @returns {Object}
 */
export const builddataobject = (calEvent, jsData, startdatetime) => {
    if (startdatetime !== null) {
        ({ jsData } = calEvent);
        calEvent.start = (startdatetime / 1000);
        calEvent.end = (calEvent.start + (30 * 60));
        calEvent.description = '';
    }
    calEvent.AllTutors = jsData.lecturers;
    calEvent.AllTags = jsData.tags;
    calEvent.AllSites = jsData.sites;
    calEvent.AllGroups = jsData.groups;
    calEvent.AllAttendees = jsData.attendees;
    calEvent.nested_locations = jsData.nested_locations;
    calEvent.tutors_select_innerhtml = buildstutorselectinnerhtml(calEvent);
    calEvent.tags_select_innerhtml = buildstagselectinnerhtml(calEvent);
    calEvent.sites_select_innerhtml = buildssitesselectinnerhtml(calEvent);
    calEvent.attendee_select_innerhtml = buildsattendeesselectinnerhtml(calEvent);
    calEvent = buildgroupevents(calEvent);
    if (typeof (window.gData) === 'undefined') {
        window.gData = {};
    }
    window.gData.calEvent = calEvent;
    return calEvent;
};


/**
 * Transform tutors from the calendar event into an array of option elements
 *
 * @function buildstutorselectinnerhtml
 *
 * @param {object} event (global data)
 * @returns {HTMLOptionElement[]}
 *
 * @see {@link module:components/dropdown.dropdown dropdown}
 */
export const buildstutorselectinnerhtml = (event) => {
    const tutors = [];
    const preselected = Object.values(event.tutors).map((tutor) => tutor.id);

    tutors.push(...Object.values(event.tutors).map(
        ({ id, title, firstname, lastname, deleted }) => ({
            id,
            text: `${title} ${firstname[0].toUpperCase()} ${lastname}`,
            deleted,
        })));

    tutors.push(...Object.values(event.AllTutors).map(
        ({ id, name, deleted }) => ({
            id,
            text: name,
            deleted,
        })));

    return dropdown(tutors.map((tutor) => ({
        ...tutor,
        text: `${tutor.text + ((tutor.deleted) ? ' (Account Inactive)' : '' )}`,
    })), preselected);
};

/**
 * @function buildstagselectinnerhtml
 *
 * @param {object} event (global data)
 * @returns {HTMLOptionElement[]}
 *
 * @see {@link module:components/dropdown.dropdown dropdown}
 */
export const buildstagselectinnerhtml = (event) => {
    const tags = Object.values(event.AllTags).map((tag) => ({
        id: tag.id,
        text: tag.name,
    }));

    return dropdown(tags, event.tags.map((tag) => tag.tagid));
};

/**
 * Transform sites from the calendar event into an array of option elements
 *
 * @function buildssitesselectinnerhtml
 * @param {object} calEvent (global data)
 * @returns {HTMLOptionElement[]}
 *
 * @see {@link module:components/dropdown.dropdown dropdown}
 */
export const buildssitesselectinnerhtml = (calEvent) => {
    const sites = Object.values(calEvent.AllSites)
        .map((site) => ({
            id: site.id,
            text: site.title,
        }));
    return dropdown(
        sites,
        Object.values(calEvent.sites).map((site) => site.siteid)
    );
};

/**
 * Transform attendees from the calendar event into an array of option elements
 *
 * @function buildsattendeesselectinnerhtml
 * @param {object} calEvent (global data)
 * @returns {HTMLOptionElement[]}
 *
 * @see {@link module:components/dropdown.dropdown dropdown}
 */
export const buildsattendeesselectinnerhtml = (calEvent) => {
    //Set `calEvent.attendees` to be an empty array if it isn't already an array..
    if (typeof calEvent.attendees === 'undefined') {
        calEvent.attendees = [];
    }

    //Flatten pre-selected attendees into a a linear array of IDs
    const existing_attendees = Object.values(calEvent.attendees)
        .map((attendee) => `U|${attendee.a_attendee_id}`);

    const attendees = [
        ...Object.values(calEvent.AllGroups).map((group) => ({
            id: `G|${group.id}`,
            text: `Group: ${group.name} (${group.acyr})`,
        })),
        ...Object.values(calEvent.AllAttendees).map((user) => ({
            id: `U|${user.id}`,
            text: `${user.name} (${user.email})`,
        })),
    ];

    return dropdown(attendees, existing_attendees);
};

/**
 * Show Event Details
 *
 * @param {object} calEvent
 * @param {object} jsData
 * @param {object} jsEvent
 * @param {object} view
 * @param {number} createdate
 * @param {string} baseurl
 *
 */
function showEventDetails(calEvent, jsData, jsEvent, view, createdate, baseurl, startdatetime) {
    var eventDetailsModal = $('#eventdetails'),
        // Drop-down boxes
        buildings_combo = eventDetailsModal.find('#selBuilding_Chosen'),
        room_list = eventDetailsModal.find('#selRoom_Chosen'),
        tag = eventDetailsModal.find('#selTag_Chosen'),
        tutor = eventDetailsModal.find('#selTutor_Chosen'),
        site = eventDetailsModal.find('#selSite_Chosen'),
        attendees = eventDetailsModal.find('#selAttendees_Chosen');

    calEvent.groupteachingevents = false;
    // only new events will have this value passed in !
    if (typeof (startdatetime) === 'undefined') {
        var startdatetime = null,
            eventmode = "Edit";
    } else {
        var eventmode = "New";
    }

    calEvent = builddataobject(calEvent, jsData, startdatetime);

    ////////////////////////////////////////////////////////////////////
    //                              Tags                              //
    ////////////////////////////////////////////////////////////////////
    tag.empty()
        .append(calEvent.tags_select_innerhtml)
        .chosen({ width: "500px" })
        .trigger('liszt:updated');

    ////////////////////////////////////////////////////////////////////
    //                              Sites                             //
    ////////////////////////////////////////////////////////////////////
    site.empty()
        .append(calEvent.sites_select_innerhtml)
        .chosen({ width: "500px" })
        .trigger('liszt:updated');

    ////////////////////////////////////////////////////////////////////
    //                            Locations                           //
    ////////////////////////////////////////////////////////////////////
    var buildings_html = $.map(calEvent.nested_locations, function (location, building_name) {
        return '<option value="' + building_name + '">' + building_name + '</option>';
    }).join('');

    buildings_combo.empty()
        .append('<option value="">Please Select a Building</option>')
        .append(buildings_html)
        .chosen({ width: "250px" })
        .bind('change liszt:updated', function () {
            var rooms_html = '',
                building_name = buildings_combo.find('option:selected').text();

            if ($(this).val() != '') {
                //Turn our array of rooms into an HTML string of <option> tags that we can put into our select list
                rooms_html += $.map(calEvent.nested_locations[building_name], function (location) {
                    return '<option value="' + location.id + '">' + location.room + '</option>';
                }).join('');
                room_list.removeAttr('disabled', true);
            } else {
                room_list.attr('disabled', true);
            }

            room_list.empty()
                .append('<option value="">Please Select a Room</option>')
                .append(rooms_html)
                .chosen({ 'width': '250px' })
                .trigger('liszt:updated');
        })
        .trigger('liszt:updated');


    if (eventmode == 'Edit') {
        if (calEvent.locations_id > 0 && calEvent.isgroupteaching === 0) {
            var rooms = _.flattenDeep(_.values(calEvent.nested_locations));

            //Search through our list of locations so that we can set the value of the select boxes
            var selected_location = _.find(rooms, function (location) {
                return location.id == calEvent.locations_id;
            });

            buildings_combo
                .val(selected_location.building)
                .trigger('liszt:updated');

            room_list
                .val(selected_location.id)
                .trigger('liszt:updated');
        }
    } else {
        $([buildings_combo, room_list]).val('');
    }


    ////////////////////////////////////////////////////////////////////
    //                             Tutors                             //
    ////////////////////////////////////////////////////////////////////
    tutor.empty()
        .append(calEvent.tutors_select_innerhtml)
        .chosen({ width: "500px" })
        .trigger('liszt:updated');


    ////////////////////////////////////////////////////////////////////
    //                           Attendees                            //
    ////////////////////////////////////////////////////////////////////
    attendees.empty()
        .append(calEvent.attendee_select_innerhtml)
        .chosen({ width: '500px', search_contains: true })
        .trigger('liszt:updated');

    if (parseInt(calEvent.published) === 1) {
        $('#published-status em').html('This event is visible to students');
    }
    if (parseInt(calEvent.cancelled) === 1) {
        $('#published-status em').html('This event has been cancelled');
    }

    var audience = this.getattributes('audience'),
        acyr = this.getattributes('acyr'),
        dpyr = this.getattributes('pgyr');

    gt_buildgrid(calEvent);
    $('#modal-from-dom').hide(true);

    $('#popup').bind('hide', function (event) {
        togglemainbuttons(false);
    });

    $('#popup').bind('show', function (event) {
        togglemainbuttons(true);
    });

    $('#btn-popup-close').bind('click', function (e) {
        togglemainbuttons(false);
        $('#popup').modal('hide');
    });

    $('#btn-copy').bind('click', function (e) {
        copyevent(calEvent);
    });

    $('#btn-delete').bind('click', function (e) {
        if (confirm('Are you sure you want to delete this event?')) {
            deleteevent(calEvent, deleteeventurl);
        }
        return false;
    });

    $('#btn-ok').bind('click', function (e) {
        var formstate = true;
        if (validate_title($('#title').val()) === false) {
            formstate = false;
            e.preventDefault();
        }
        if (validate_description($('#description').text()) === false) {
            formstate = false;
            e.preventDefault();
        }
        if (validate_start_end() === false) {
            formstate = false;
            e.preventDefault();
        }
        if (formstate === true) {
            $("form:first").submit();
            e.preventDefault();
        }
    });

    calEvent.start = parseInt(calEvent.start);
    calEvent.end = parseInt(calEvent.end);

    $('#degreeprogyr').val(getattributes('pgyr'));
    $('#acyr').val(getattributes('acyr'));
    $('#audience').val(getattributes('audience'));
    if (eventmode === "Edit") {
        $('#modal-event-title').html("Edit " + calEvent.title);
    } else {
        $('#modal-event-title').html("New Event");
    }
    $('#title').val(calEvent.title);
    $('#lr-resource-title').html(calEvent.title); /** hacky fix to disaplay something relevant **/
    if ((calEvent.end - calEvent.start) < 86400) {
        $('#start').val(moment.unix(calEvent.start).format('DD-MM-YYYY HH:mm')); // + ' - ' + moment.unix(calEvent.end).format('HH:mm'));
        $('#end').val(moment.unix(calEvent.end).format('DD-MM-YYYY HH:mm'));// + ' - ' + moment.unix(calEvent.end).format('HH:mm'));
    } else {
        $('#start').val(moment.unix(calEvent.start).format('DD-MM-YYYY HH:mm'));// + ' - ' + moment.unix(calEvent.end).format('dd/MM/yyyy hh:mm'));
        $('#end').val(moment.unix(calEvent.end).format('DD-MM-YYYY HH:mm'));// + ' - ' + moment.unix(calEvent.end).format('dd/MM/yyyy hh:mm'));
    }

    $('#id').val(calEvent.id);
    $('#audience').val(calEvent.audience);

    $('#flags_allday').val(calEvent.isallday);
    if (calEvent.isallday === 1) {
        var allday = 'isallday_true';
    } else {
        var allday = 'isallday_false';
    }
    toggleflags(allday);
    $('#flags_prs').val(calEvent.requireprs);
    if (calEvent.requireprs === 1) {
        var prs = 'prsrequired_true';
    } else {
        var prs = 'prsrequired_false';
    }
    toggleflags(prs);

    $('#flags_cancelled').val(calEvent.cancelled);
    if (calEvent.cancelled === 1) {
        var cancelled = 'cancelled_true';
    } else {
        var cancelled = 'cancelled_false';
    }
    toggleflags(cancelled);

    $('#flags_confirmed').val(calEvent.confirmed);
    if (calEvent.confirmed === 1) {
        var confirmed = 'confirmed_true';
    } else {
        var confirmed = 'confirmed_false';
    }
    toggleflags(confirmed);

    $('#flags_published').val(calEvent.published);
    if (calEvent.published === 1) {
        var published = 'published_true';
    } else {
        var published = 'published_false';
    }

    toggleflags(published);


    if (parseInt(calEvent.isgroupteaching) === 1) {
        var gt = 'groupteaching_true';
        toggleAttendeestag(calEvent);
    } else {
        var gt = 'groupteaching_false';
        toggleAttendeestag(calEvent);
    }
    toggleflags(gt);
    $('.datepicker').datetimepicker({
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minuteStep: 15,
        forceParse: 0,
        daysOfWeekDisabled: '0,6',
        format: "dd-mm-yyyy hh:ii"
    });
    setIsAllDayEvent(calEvent);
    setIsPRSRequired(calEvent);
    setIsCancelled(calEvent);
    setIsConfirmed(calEvent);
    setIsPublished(calEvent);
    setGroupTeaching(calEvent);

    $('#isalldaybtnset label, #prsreqbtnset label, #cancelledbtnset label, #confirmedbtnset label, #publishedbtnset label, #groupteachingbtnset label, ' +
        '#progyrradio label, #acyrradio label').unbind().on('click', calEvent, function () {
            var id = $(this).attr('for');
            var btnset = id.split('_');
            btnset = btnset[0];

            if (id === 'groupteaching_false') {
                if (!confirm('Are you sure you want to switch off group teaching?')) {
                    return false;
                }
            }

            // If the input is disabled then do nothing
            if (!$('input#' + id).prop('disabled')) {
                $("label[for*='" + btnset + "']").removeClass('checked');
                $(this).addClass('checked');
                $("input[name='" + btnset + "']").removeAttr('checked').button('refresh');
                $('input#' + id).attr('checked', 'checked');
                toggleAttendeestag(calEvent);
                toggleflags(id);
            }
        });

    toggleAttendeestag(calEvent);

    $('#terminator').html("</form>");

    var $ta = $('#description'),
        w5ref = $ta.data('wysihtml5');

    if (w5ref) {
        w5ref.editor.setValue(calEvent.description);
    } else {
        $ta.html(calEvent.description);
    }
    eventDetailsModal.modal('show');
}

/**
 * @version   Nov-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 *
 *
 *
 * @copyright 2013 onwards University of Aberdeen
 *
 * @param {object} calEvent
 *
 */
function toggleAttendeestag(calEvent) {
    var GT = "empty";
    if ($('#lbl_groupteaching_false').hasClass('checked')) {
        $('#lbl_groupteaching_true').removeClass('checked');
        GT = false;
    }
    if ($('#lbl_groupteaching_true').hasClass('checked')) {
        $('#lbl_groupteaching_false').removeClass('checked');
        GT = true;
    }
    if (typeof (calEvent) !== 'undefined' && calEvent !== null) {
        if (GT === true) {
            calEvent = buildgroupevents(calEvent);
            $('.attendee_std').hide();
            $('.attendee_adv').show();
            $('#flags_groupteaching').val(1);
            $('#selBuilding_Chosen').hide();
            $('#selRoom_Chosen').hide();
            $('#selTutor_Chosen_chzn').hide();
            $('#selTutor_Chosen').hide();
            $('.hide_on_gt').hide();
            $('#hide_on_gt').toggle();
            $("#tabs-details").attr('class', 'tab-pane active');
            $("#tabs-tags").attr('class', 'tab-pane');
            $("#tabs-attendees").attr('class', 'tab-pane');
            $("#tabs-description").attr('class', 'tab-pane');
            $("#event-information-tabs").html('<li class="active"><a href="#tabs-details" data-toggle="tab">Event Details</a></li><li><a href="#tabs-description" data-toggle="tab">Description</a></li><li><a href="#tabs-attendees" data-toggle="tab">Attendees</a></li><li><a href="#tabs-tags" data-toggle="tab">Tags</a></li>');
            createblankgteventstable(calEvent);
            attachbuttons(calEvent);
            $('#btn-0').click();
        } else {
            $('.attendee_std').show();
            $('.attendee_adv').hide();
            $('#flags_groupteaching').val(0);
            $('.hide_on_gt').show();
            $('#hide_on_gt').show();
            $('#hide_on_gt').toggle();
            $('#selTutor_Chosen_chzn').show();
        }
    }
}


/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} calEvent
 * @param {string} url
 * @returns {void}
 */
function deleteevent(calEvent, url) {
    bootbox.confirm("Are you sure?", function (result) {
        if (result === true) {
            var id = calEvent.id; // get current event ID
            $.ajax({
                type: "GET",
                url: url,
                data: { eventid: id }
            });
            bootbox.alert("Event :" + calEvent.title + "  Deleted !");
        }
        ;
    });
}
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} alecturers
 * @param {object} attendees
 * @returns {void}
 */
function clearform(alecturers, attendees) {
    $('#id').val(null);
    $('#title').val('');
    $('#start').val('');
    $('#end').val('');
    $('#description').html("<textarea class='input-xxlarge' placeholder='Add any extra information here' name='description' id='description'></textarea>");
    clearlecturers(alecturers);
    cleanattendees(attendees);
}
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} alecturers
 * @returns {void}
 */
function clearlecturers(alecturers) {
    $('#selTutor_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge" multiple style="width:320px;" tabindex ="-1" id="selTutor_Chosen" name ="lecturers"></select>');
    $('#selTutor_Chosen').val('');
    $('#selTutor_Chosen').trigger('liszt:updated');
    $('#selTutor_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge" multiple style="width:320px;" tabindex ="-1" id="selTutor_Chosen" name ="lecturers">' + getcleanlecturers(alecturers) + '</select>');
    $('#selTutor_Chosen').trigger('liszt:updated');
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} attendees
 * @returns {void}
 */
function clearattendees(attendees) {
    $('#selTutor_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge" multiple style="width:320px;" tabindex ="-1" id="selTutor_Chosen" name ="lecturers"></select>');
    $('#selTutor_Chosen').val('');
    $('#selTutor_Chosen').trigger('liszt:updated');
    $('#selTutor_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge" multiple style="width:320px;" tabindex ="-1" id="selTutor_Chosen" name ="lecturers">' + getcleanlecturers(alecturers) + '</select>');
    $('#selTutor_Chosen').trigger('liszt:updated');
}
// clean list of lecturers
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} attendees
 * @returns {void}
 */
function cleanattendees(attendees) {
    var length = attendees.length;
    var retval = "";
    for (var i = 0; i < length; i++) {
        retval = retval + "<option  value ='" + attendees[i].id + "'>" + attendees[i].name + "</option>";
    }
    $('#selAttendees_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge multiple style="width:200px;" tabindex ="-1" id="selAttendees_Chosen" name="attendees">' + retval + '</select>')
    $('#selAttendees_Chosen').chosen({ width: "500px" });
    $('#selAttendees_Chosen').trigger('liszt:updated');
}

/**
 * set the is all day event value on screen to reflect the stored value
 *
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 *
 * @param {object} event
 * @returns {void}
 */
function setIsAllDayEvent(event) {
    if (parseInt(event.isallday) === 1) {
        $('#flags_allday').val(1);
        setRadioEventValues('isallday_true');
    } else {
        $('#flags_allday').val(0);
        setRadioEventValues('isallday_false');
    }
}

/**
 * set the is all day event value on screen to reflect the stored value
 *
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 *
 * @param {object} event
 * @returns {void}
 */
function setIsAllDayEventBasic(value) {
    if (parseInt(value) === 1) {
        $('#flags_allday').val(1);
        setRadioEventValues('isallday_true');
    } else {
        $('#flags_allday').val(0);
        setRadioEventValues('isallday_false');
    }
}
/**
 * set the PSR value on screen to reflect the stored value
 *
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 *
 * @param {object} event
 * @returns {void}
 */
function setIsPRSRequired(event) {
    if (parseInt(event.requireprs) === 1) {
        $('#flags_prs').val(1);
        setRadioEventValues('prsrequired_true');
    } else {
        $('#flags_prs').val(0);
        setRadioEventValues('prsrequired_false');
    }
}

/**
 * set the PSR value on screen to reflect the stored value
 *
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 *
 * @param {object} event
 * @returns {void}
 */
function setIsPRSRequiredBasic(value) {
    if (parseInt(value) === 1) {
        $('#flags_prs').val(1);
        setRadioEventValues('prsrequired_true');
    } else {
        $('#flags_prs').val(0);
        setRadioEventValues('prsrequired_false');
    }
}

/**
 * set the published value on screen to reflect the stored value
 *
 * @param {object} event
 * @returns {void}
 */
function setIsPublished(event) {
    if (parseInt(event.published) === 1) {
        $('#flags_published').val(1);
        setRadioEventValues('published_true');
        // Disable the confirmed button as logic on the REST server prevents a change to confirmed from published state
        $('#status_confirmed').prop("disabled", true);
        $('#confirmedbtnset label').addClass('ui-state-disabled');
    } else {
        $('#flags_published').val(0);
        setRadioEventValues('published_false');
    }
}

/**
 * set the confirmed value on screen to reflect the stored value
 *
 * @param {object} event
 * @returns {void}
 */
function setIsConfirmed(event) {
    if (parseInt(event.confirmed) === 1) {
        $('#flags_confirmed').val(1);
        setRadioEventValues('confirmed_true');
    } else {
        $('#flags_confirmed').val(0);
        setRadioEventValues('confirmed_false');
    }
}

/**
 * set the cancelled value on screen to reflect the stored value
 *
 * @param {object} event
 * @returns {void}
 */
function setIsCancelled(event) {
    if (parseInt(event.cancelled) === 1) {
        $('#flags_cancelled').val(1);
        setRadioEventValues('cancelled_true');
    } else {
        $('#flags_cancelled').val(0);
        setRadioEventValues('cancelled_false');
    }
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function setGroupTeaching(event) {
    if (parseInt(event.isgroupteaching) === 1) {
        $('#flags_groupteaching').val('1');
        setRadioEventValues('groupteaching_true');

    } else {
        $('flags_groupteaching').val('0');
        setRadioEventValues('groupteaching_false');
    }
}
/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function setGroupTeachingBasic(value) {
    if (parseInt(value) === 1) {
        $('#flags_groupteaching').val('1');
        setRadioEventValues('groupteaching_true');
    } else {
        $('flags_groupteaching').val('0');
        setRadioEventValues('groupteaching_false');
    }
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string} id
 */
function setRadioEventValues(id) {
    var radio = $('#' + id),
        tmp = id.split("_");

    if (tmp[0] === ALL_DAY || tmp[0] === PRS_REQUIRED || tmp[0] === GROUP_TEACHING || tmp[0] === CANCELLED || tmp[0] === CONFIRMED || tmp[0] === PUBLISHED) {
        $("label[for='" + tmp[0] + "_false" + "']").removeClass('checked');
        $("label[for='" + tmp[0] + "_true" + "']").removeClass('checked');
    }

    $("label[for='" + id + "']")
        .removeClass('checked')
        .addClass('checked');

    /*if(typeof radio !== 'undefined'){
        radio[0].checked = true;
        radio.button("refresh");
    }*/
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} calEvent
 *
 */
function copyevent(calEvent) {
    $('#id').val(''); // wipe id so new record is created !!!
    $('#modal-event-title').html(calEvent.title);
    $('#title').val(calEvent.title + "(Copy)");
    $('#degreeprogyr').val = getattributes('pgyr');
    $('#acyr').val = getattributes('acyr');
    $('#audience').val = getattributes('audience');
}

// clean list of lecturers
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {object} lecturers}
 */
function getcleanlecturers(lecturers) {
    var length = lecturers.length;
    var retval = "";
    for (var i = 0; i < length; i++) {
        retval = retval + "<option  value ='" + lecturers[i].id + "'>" + lecturers[i].name + "</option>";
    }
    $('#selTutor_Chosen').html('<select data-placeholder="Find" class="chzn-container chzn-container-multi chzn-with-drop chzn-container-active input-xxlarge" multiple style="width:350px;" tabindex ="-1" id="selTutor_Chosen" name ="lecturers">' + retval + '</select>');
    $('#selTutor_Chosen').chosen({ width: "500px" });
    $('#selTutor_Chosen').trigger('liszt:updated');
    return retval;
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {ajax} x
 * @returns {void}
 */
function validate(x) {
    x.success(function (validationdata) {
        var len = validationdata.length;
        var item;
        for (var i = 0; i < len; i++) {
            item = validationdata[i]
        }
    });
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string} validationurl
 * @returns {void}
 */
function checkformdata(validationurl) {
    var formvalues = getfromcontents();
    var callajax = validateevents(formvalues, validationurl);
    var retval = validate(callajax);
    return retval;
}
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string|object} formcontents
 * @param {string} validationurl
 * @returns {@exp;$@call;ajax}
 */
function validateevents(formcontents, validationurl) {
    return $.ajax({
        type: "GET",
        async: false,
        url: validationurl,
        data: { data: formcontents }
    });
}
/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function getgtevents(eventid, gturl) {
    var id = parseInt(eventid.id); // get current event ID
    return $.ajax({
        type: "GET",
        async: false,
        url: gturl,
        data: { eventid: id },
        dataType: "json"
    });
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param string attribute this is the attribute that you require
 *                              acyr       : accidemic year
 *                              pgyr       : degree program year
 *                              audience   : the acdience
 * @returns {string} or false for a fail.
 */
function getattributes(attribute) {
    var jsacyrregex = /\d{4} /;                          // regex for acidemic year
    var jspgryregex = /\d{1} /;                         // regex for degree program year
    var jsaudienceregex = /<a[^>]*>(.*?)<\/a>/;        // regex for audience
    var listItems = $('li.active');
    var rawacyr = listItems[0];
    var acyr = jsacyrregex.exec(rawacyr.innerHTML);
    var rawpgyr = listItems[1];
    var pgyr = jspgryregex.exec(rawpgyr.innerHTML);
    var rawacdience = listItems[2];
    var audience = jsaudienceregex.exec(rawacdience.innerHTML);
    if (attribute === "acyr") {
        var acyr = 0;
        var d = new Date();
        var n = d.getMonth();
        if (n < 7) {
            acyr = (acyr[0] - 1);
        }

        return acyr;
    }
    if (attribute === 'pgyr') {
        return pgyr[0];
    }
    if (attribute === 'audience') {
        return audience[1];
    }
    return false;
}
//-------------------form validation !!!
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 *
 * @param {string} input
 * @returns {Boolean}
 */
function validate_title(input) {
    if (input.length > 1) {
        $('.title_error').hide();
        return true;
    } else {
        $('.title_error').hide();
        $('#title').before('<span class="title_error"> Please ensure that the title has at least some characters !</span>');
        return false;
    }
}
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string} input
 * @returns {Boolean}
 */
function validate_description(input) {
    if (input.length > 3) {
        $('.description_error').hide();
        return true;
    } else {
        $('.description_error').hide();
        return true;
    }
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string} input
 * @returns {Boolean}
 */
function validate_tutor(input) {
    if (input.length > 3) {
        $('.tutor_error').hide();
        return true;
    } else {
        $('#lecturer').before('<span class="tutor_error"> Please select at least one tutor</span>');
        return false;
    }
}

/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @returns {Boolean} validation pass True or False for the dates.
 */
function validate_start_end() {
    var fd = ($('#start').val().split(' '));
    var ld = ($('#end').val().split(' '));
    var date_ini = fd[0].split("-");
    var time_ini = fd[1].split(":");
    var date_end = ld[0].split("-");
    var time_end = fd[1].split(":");
    var sd = new Date(date_ini[2], date_ini[1] - 1, date_ini[0], time_ini[0], time_ini[1]);
    var ed = new Date(date_end[2], date_end[1] - 1, date_end[0], time_end[0], time_end[1]);
    var errortext = "";
    var err = false;
    if (isNaN(sd)) {
        err = true;
        errortext = "Start date is not valid ";
    }
    if (isNaN(ed)) {
        err = true;
        errortext = errortext + "End date is not valid ";
    }
    if (sd > ed) {
        err = true;
        errortext = errortext + "Start should be prior to end ";
    }
    if (err === true) {
        $('.date_error').hide();
        $('label[for="dtpicker-start"]').after('<span class="date_error">' + errortext + '</span>');
        return false;
    } else {
        $('.date_error').hide();
        return true;
    }
}
/**
 * @version   Aug-2013
 * @author    <j.boag@abdn.ac.uk> James Boag
 *
 * @copyright 2012 onwards University of Aberdeen
 * @param {string} id
 * @returns {void}
 */
function toggleflags(id) {
    var bits = id.split('_');
    var flag = bits[0];
    var value = bits[1];
    if (flag === ALL_DAY) {
        if (value === 'true') {
            $('#flags_allday').val(1);
        } else {
            $('#flags_allday').val(0);
        }
    }
    if (flag === PRS_REQUIRED) {
        if (value === 'true') {
            $('#flags_prs').val(1);
        } else {
            $('#flags_prs').val(0);
        }
    }

    if (flag === CANCELLED) {
        if (value === 'true') {
            $('#flags_cancelled').val(1);
        } else {
            $('#flags_cancelled').val(0);
        }
    }

    if (flag === CONFIRMED) {
        if (value === 'true') {
            $('#flags_confirmed').val(1);
        } else {
            $('#flags_confirmed').val(0);
        }
    }

    if (flag === PUBLISHED) {
        if (value === 'true') {
            $('#flags_published').val(1);
        } else {
            $('#flags_published').val(0);
        }
    }
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function cleanuparray(calEvent) {
    var backuporigarray = calEvent.groupteachingevents;
    var newarray = Array();
    var freshindex = 0;
    var len = calEvent.groupteachingevents.length;
    for (var i = 0; i < len; i++) {
        if (calEvent.groupteachingevents[i] !== null) {
            newarray[freshindex] = calEvent.groupteachingevents[i];
            freshindex++;
        }
    }
    calEvent.groupteachingevents = newarray;
    return calEvent;
}


/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function wizardbuttons() {
    return '<div><span><div id="btn-0"></div><div id="btn-1"></div><div id="btn-2"></div><div id="btn-3"></div><div id="btn-4"></div> </span></div>';
}


/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function attachbuttons(calEvent) {
    $('#btn-0').html('<button class="btn btn-primary" id="btn-0" type="button" name="btn-0" value="Group">Group</button>').click(function (e) {
        populatepopup(calEvent);
        e.preventDefault();
    });
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function gt_groupchange() {
    window['wzData'].GSelectedGroupText = $("#popup_group_list option:selected").text();
    window['wzData'].GSelectedGroupValue = $("#popup_group_list option:selected").val();
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function gt_buildingchange() {
    window['wzData'].GSelectedBuildingText = $("#popup_building_list option:selected").text();
    window['wzData'].GSelectedBuildingValue = $("#popup_building_list option:selected").val();
    new_popup_rooms();
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function gt_tutorchange() {
    var tmp_text = "";
    var tmp_val = "";
    $("#popup_tutor_list option:selected").each(function (index) {
        tmp_text = tmp_text + $(this).text() + '~';
        tmp_val = tmp_val + $(this).val() + '~';
    });
    var vallen = tmp_val.length;
    var txtlen = tmp_text.length;
    window['wzData'].GSelectedTutorValue = tmp_val.substring(0, (vallen - 1));
    window['wzData'].GSelectedTutorText = tmp_text.substring(0, (txtlen - 1));
}

/**
 * GT Build Grid
 *
 * Build a grid of group teaching events
 *
 * @param  {object} calEvent A calendar event object
 * @returns {void}
 */
function gt_buildgrid(calEvent) {
    var innerhtml = $.map(calEvent.groupteachingevents, function (group_teaching_record, id) {
        var group = group_teaching_record.groupid,
            grouptext = group_teaching_record.groupname,
            room = group_teaching_record.locationid,
            roomtext = group_teaching_record.locationname,
            tutor = group_teaching_record.tutorid,
            rowname = "row_" + id;

        if (tutor) {
            var tmp = group_teaching_record.tutorname.split(' '),
                tutortext = tmp[0] + ' ' + tmp[1].substr(0, 1) + ' ' + tmp[2];
        } else {
            var tutortext = 'TBC';
        }

        return '<tr id="row_' + id + '" style="height:20px">' +
            '<td id="group_' + id + '">' + grouptext + '</td>' +
            '<td id="room_' + id + '">' + roomtext + '</td>' +
            '<td id="tutor_' + id + '">' + tutortext + '</td>' +
            '<td>' +
            '<table cellpadding="2">' +
            '<tr>' +
            '<td>' +
            '<a href="javascript:save_group_teaching_modal(' + id + ')"><i class="icon-edit"></i></a>' +
            '</td>' +
            '<td>' +
            '<a href="javascript:removerow(' + id + ')"><i class="icon-trash"></i></a>' +
            '</td>' +
            '</tr>' +
            '</table>' +
            '</td>' +
            '</tr>';
    });
    $("#groupevents > tbody").append(innerhtml);
}

/**
 * Remove Row
 *
 * Remove a group teaching record
 *
 * @param {number} id The array index of the group teaching record to delete
 *
 * @returns {void}
 */
function removerow(id) {
    if (confirm("Are you sure you want to delete this item ?")) {
        $('#row_' + id).remove();
        window.gData.calEvent.groupteachingevents.splice(id, 1);
        $('#hidden_gt_values').val(pack_gt_info(window.gData.calEvent));
    }
}

/**
 * Pack GT Info
 *
 * Serializes the group teaching records for the currently selected event
 *
 * @param  {object} calEvent A calendar event object
 *
 * @returns {string}          A JSON string representing all the group teaching records for this event
 */
function pack_gt_info(calEvent) {
    var gt_info = $.map(calEvent.groupteachingevents, function (gt_info) {
        return {
            group: {
                id: (gt_info.groupid.substring(0, 2) === 'G|') ? gt_info.groupid.split('|')[1] : gt_info.groupid,
                name: gt_info.groupname
            },
            location: {
                id: gt_info.locationid,
                name: gt_info.locationname
            },
            tutor: {
                id: gt_info.tutorid,
                name: gt_info.tutorname
            }
        }
    });

    return JSON.stringify(gt_info);
}

/*
 *  clear forms (mode)
 *
 *
 * @author James Boag <j.boag@abdn.ac.uk>
 * @version November-2013
 * @copyright   Medi-CAL Unit, University of Aberdeen
 *
 * @params {string) mode (new)
 * resets all field values to default.
 */

function clearforms(mode) {
    if (mode === 'new') {
        $('#title').val('');
        $('#isallday_true').attr("checked", true);
        $('#prsrequired_true').attr('checked', true);
        $('#status_unpublished').attr('checked', true);
        $('#status_cancelled').attr('checked', false);
        $('#groupteaching_false').attr('checked', false);
        $('#description').html('');
        $('#start').datetimepicker({
            format: "dd MM yyyy - HH:ii"
        });
        $('#end').datetimepicker({
            format: "dd MM yyyy - HH:ii"
        });

    }

}

/**
 * Save Group Teaching Modal
 *
 * Displays the group teaching modal
 *
 * @param  {number} id The ID of the group teaching record to populate the form with(optional).
 * @returns {void}
 */
function save_group_teaching_modal(id) {
    var popup = $('#popup'),
        group_list = popup.find('#popup_group_list'),
        tutor_list = popup.find('#popup_tutor_list'),
        building_list = popup.find('#popup_building_list'),
        room_list = popup.find('#popup_room_list'),
        save_button = popup.find('#add_gt_event_buton'),
        calEvent = window.gData.calEvent;

    ////////////////////////////////////////////////////////////////////
    //                           Attendees                            //
    ////////////////////////////////////////////////////////////////////
    var groups_html = $.map(calEvent.AllGroups, function (group) {
        return '<option value="' + group.id + '">' + group.name + ' (' + group.acyr + ')</option>';
    }).join('');

    group_list
        .empty()
        .append(groups_html)
        .chosen({ 'width': '500px' })
        .trigger('liszt:updated');


    ////////////////////////////////////////////////////////////////////
    //                            Locations                           //
    ////////////////////////////////////////////////////////////////////
    var buildings_html = $.map(calEvent.nested_locations, function (location, building_name) {
        return '<option value="' + building_name + '">' + building_name + '</option>';
    }).join('');

    building_list.empty()
        .append('<option value="">Please Select a Building</option>')
        .append(buildings_html)
        .chosen({ 'width': '500px' })
        .bind('change liszt:updated', function () {
            var rooms_html = '',
                building_name = $(this).find('option:selected').text();

            if ($(this).val() != '') {
                //Turn our array of rooms into an HTML string of <option> tags that we can put into our select list
                rooms_html += $.map(calEvent.nested_locations[building_name], function (location) {
                    return '<option value="' + location.id + '">' + location.room + '</option>';
                }).join('');
                room_list.removeAttr('disabled', true);
            } else {
                room_list.attr('disabled', true);
            }

            room_list.empty()
                .append('<option value="">Please Select a Room</option>')
                .append(rooms_html)
                .chosen({ 'width': '500px' })
                .trigger('liszt:updated');
        })
        .trigger('liszt:updated');

    ////////////////////////////////////////////////////////////////////
    //                             Tutors                             //
    ////////////////////////////////////////////////////////////////////
    var tutors_html = $.map(calEvent.AllTutors, function (tutor) {
        if (tutor.deleted == 1) {
            tutor.name = tutor.name + ' (Account Inactive)';
        }
        return '<option value="' + tutor.id + '">' + tutor.name + '</option>';
    });

    tutor_list
        .empty()
        .append(tutors_html)
        .chosen({ 'width': '500px' })
        .trigger('liszt:updated');

    if (typeof id !== 'undefined') {
        //If the user has provided an ID, use this to populate the form

        //First grab the group teaching record that we're supposed to be editing
        var group_teaching_record = calEvent.groupteachingevents[id];

        //Now update our fields with the right values
        group_list
            .val(parseInt(group_teaching_record.groupid))
            .trigger('liszt:updated');
        //Get the full location record for our selected location. Underscore is used here
        //to flatten the object into an array so that we can use it with $.grep
        var location = $.grep(_.values(calEvent.locations), function (location) {
            return location.id == group_teaching_record.locationid;
        })[0];

        if (location) {
            building_list
                .val(location.building)
                .trigger('liszt:updated');

            room_list
                .val(location.id)
                .trigger('liszt:updated');
        }

        if (typeof group_teaching_record.tutorid !== 'undefined') {
            tutor_list
                .val(group_teaching_record.tutorid)
                .trigger('liszt:updated');
        }
    } else {
        //If they didn't provide an ID - that means that it's an add, so we want to make sure that we blank our form
        $([group_list, building_list, room_list, tutor_list])
            .val('')
            .trigger('liszt:updated');
    }

    //Make our popup visible
    popup
        .modal('show')
        .css('z-index', '3100')
        .on('show.bs.modal', function () {
            //Disable the buttons on the main form when our modal appears
            togglemainbuttons(true);
        })
        .on('hide.bs.modal', function () {
            //Clean up after ourselves and unbind all the events from the modal when it gets hidden so
            //that we don't end up with memory leaks..
            $(this).unbind();

            save_button.unbind();

            //Re-enable the buttons on the main form when we hide our modal
            togglemainbuttons(false);
        });

    //Bind the save action to the save_group_teaching_event() function to save our form data
    $(save_button).bind('click', function () {
        //Pass our form data to the save_group_teaching_event function for saving...
        save_group_teaching_event({
            groupid: group_list.find('option:selected').val(),
            groupname: group_list.find('option:selected').text(),
            locationid: room_list.find('option:selected').val(),
            locationname: building_list.find('option:selected').text() + ' - ' + room_list.find('option:selected').text(),
            tutorid: tutor_list.find('option:selected').val() || undefined,
            tutorname: tutor_list.find('option:selected').text() || undefined
        }, id);

        popup.modal('hide');
    });
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function new_popup_rooms(calEvent) {
    $('#popup_room_list').html('<select data-placeholder="Find" class="popup_room_list" tabindex ="-1" id="popup_room_list" name="popup_room_list"></select>')
        .chosen({ width: "500px" })
        .trigger('liszt:updated')
        .bind('change', function () {
            window['wzData'].GSelectedRoomText = $(this).find('option:selected').text();
            window['wzData'].GSelectedRoomValue = $(this).val();
        });
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function format_gt_buildings(allroomsurl) {
    $('#popup_room_list').html('<select name="popup_room_list">' + getlistofroomsforbuildingGTNew(allroomsurl) + '</select>');
    $('#popup_room_list').chosen({ width: "500px" });
    $('#popup_room_list').trigger('chosen:updated');
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function getlistofroomsforbuildingGTNew() {
    var allroomsurl = window['Gallroomsurl'];
    var currentbuilding = $('#popup_building_list :selected').text();
    var currentroom = $('#popup_room_list :selected').text();
    var retval = "";
    var selected = "";
    var strroom = currentroom;
    var callajax = getrooms(allroomsurl);
    if (callajax.success(function (realData) {
        var buildings = realData;
        var length = buildings.length;
        for (var i = 0; i < length; i++) {
            if ($.trim(currentbuilding) === $.trim(buildings[i].building)) {
                if (strroom === buildings[i].room) {
                    selected = " selected='selected'  ";
                } else {
                    selected = "";
                }
                retval = retval + "<option " + selected + " value='" + buildings[i].id + "'>" + buildings[i].room + "</option>";
            }
        }

    },
        callajax.fail(function () {
        })
    ))
        ;
    return retval;
}

/**
 * Save GT event
 *
 * Saves data related to the group teaching aspect of an event
 *
 * @param {object} form_data The form data to save
 * @param {number} id        The ID of the group teaching record to modfiy. Add is assumed if omitted.
 *
 * @returns {void}
 */
function save_group_teaching_event(form_data, id) {
    var calEvent = window.gData.calEvent;

    //Set `calEvent.groupteachingevents` to be an empty array if it isn't already an array..
    if (calEvent.groupteachingevents.constructor !== Array) {
        calEvent.groupteachingevents = [];
    }

    if (form_data.groupname.indexOf('Group: ') > -1) {
        var groupname = form_data.groupname.slice(8);
    } else {
        var groupname = form_data.groupname;
    }

    if (typeof id !== 'undefined') {
        //Replace the contents of array at the specified index with our new form data
        //we check the type of `id` against `undefined` rather than just checking to see if it's truthy
        //because sometimes it might be equal to 0 which is falsy, but in our case we want to just use it as
        //as the integer 0, rather than a boolean value
        calEvent.groupteachingevents[id] = form_data;
    } else {
        //Push the new group teaching information on to the array of existing group teaching events
        calEvent.groupteachingevents.push(form_data);
    }

    //Overwrite the global calEvent variable with our local calEvent variable(with the new group teaching events added)
    window.gData.calEvent = calEvent;

    //Update the group teaching events table with the data from the calEvent object
    createblankgteventstable(calEvent);
}


/**
 * Toggle main buttons
 *
 * @author James Boag <j.boag@abdn.ac.uk>
 * @version November-2013
 * @copyright   Medi-CAL Unit, University of Aberdeen
 *
 * @param {boolean} state
 *
 * if state === TRUE we disable the main ok publish delete copy and close
 *                   button this is called when we pop the popup
 *                   ontop of the main dialog
 *
 * if state === FALSE we enable these button again
 */

function togglemainbuttons(state) {
    if (state) {
        $('#btn-ok').prop("disabled", true);
        $('#btn-publish').prop("disabled", true);
        $('#btn-delete').prop("disabled", true);
        $('#btn-close').prop("disabled", true);
        $('#btn-copy').prop("disabled", true);
    } else {
        $('#btn-ok').removeProp("disabled");
        $('#btn-publish').removeProp("disabled");
        $('#btn-delete').removeProp("disabled");
        $('#btn-close').removeProp("disabled");
        $('#btn-copy').removeProp("disabled");
    }
}


/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 *
 *  this function creates the blank table to hold group events
 *  on the attendees tab
 */

function createblankgteventstable(calEvent) {
    var innerwidth = (528 - getScrollBarWidth());
    var groupwidth = $('#grouphdrwidth').width();
    var locationwidth = $('#locationhdrwidth').width();
    var tutorwidth = $('#tutorhdrwidth').width();
    var hdriconswidth = $('#iconhdrwidth').width();
    var innericonswidth = (hdriconswidth - getScrollBarWidth);
    window.gData.calEvent = calEvent;
    var html = "";
    html = html + "  <div class='gt_events_table_all' id='gt_events_table_all' style='height:380px'>";
    html = html + "  <table border = '1px' cellpadding = '0'  width ='100%'><tr><td>";
    html = html + "  <table border = '1px' cellpadding = '0'  width ='100%'><tr><td>";
    html = html + "  <table id='groupeventshdr' border ='0px' cellpadding ='5' width ='100%'>";
    html = html + "   <thead>";
    html = html + "       <tr>";
    html = html + "         <th border ='0px' align ='left'>Group</th>";
    html = html + "         <th  border ='0px'>Location</th>";
    html = html + "         <th  border ='0px'>Tutor</th>";
    html = html + "         <th  border ='0px'><table  border ='0px'><tr><td></td><td></i></td></tr></table></th></th>";
    html = html + "      </tr>";
    html = html + "    </thead>";
    html = html + "  </table>";
    html = html + "   </td></tr></table>";
    html = html + "  <div class='gt_events_table' id='gt_events_table' style='overflow-y: scroll; height:350px'>";
    html = html + "    <table id = 'groupevents' border ='1px' cellpadding ='5' width ='100%' >";
    html = html + "     <tbody>";
    html = html + "     </tbody>";
    html = html + "     </table>";
    html = html + "  </div>";
    html = html + "  <table border = '1px' cellpadding = '0'  width ='100%'><tr><td>";
    html = html + "  <table id = 'groupeventsftr' border ='0px' cellpadding ='5' width ='100%' >";
    html = html + "  <tfoot>";
    html = html + "     <tr>";
    html = html + "       <td colspan ='4' align = 'center' border ='0px'>";
    html = html + "         <a href='javascript:save_group_teaching_modal()'  class='btn btn-success'>";
    html = html + "           <i class='icon-plus' ></i> ";
    html = html + "         </a> Add Group";
    html = html + "       </td>";
    html = html + "       </tr>";
    html = html + "   </tfoot>";
    html = html + " </table>";
    html = html + "   </td></tr></table>";
    html = html + " </td></tr></table>";
    html = html + " </div>";
    $('#gt_events_table_all').html(html);

    gt_buildgrid(calEvent);

    $('#hidden_gt_values').val(pack_gt_info(calEvent));
}

/**
 *  @author James Boag <j.boag@abdn.ac.uk>
 *  @version December 2013
 *  @copyright  Medi-CAL Unit, University of Aberdeen
 */
function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
}
