/**
 * @type {object}
 * @memberof module:Constants
 * @property {string} ALL_DAY This event is an "all day" event
 * @property {string} PRS_REQUIRED PRS handsets are required for this event
 * @property {string} GROUP_TEACHING The event is a group teaching event
 */
export const FLAGS = {
    ALL_DAY: 'isallday',
    PRS_REQUIRED: 'prsrequired',
    GROUP_TEACHING: 'groupteaching',
};
