/**
 * @type {object}
 * @memberof module:Constants
 * @property {string} PUBLISHED The event is confirmed and is publicly available
 * @property {string} CONFIRMED The event has been confirmed but is not currently publicly available
 * @property {string} CANCELLED The event was cancelled but is still displayed on calendars for informational reasons
 */
export const STATUS = {
    PUBLISHED: 'published',
    CONFIRMED: 'confirmed',
    CANCELLED: 'cancelled',
};
