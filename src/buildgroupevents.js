/**
 * Link tuors with locations and groups for group teaching events
 *
 * @function buildgroupevents
 *
 * @param {Object[]}  event  An array of items to convert into a dropdown
 * @param {Number}  event.isgroupteaching  Determines wether or not this is a group teaching event. `1` for `true`, `0` for `false`
 *
 * @param {Object}  event.attendees  The attendees(users or groups) already attached to this event
 * @param {Number}  event.attendees[].a_id  Primary key ID of the pivot table record
 * @param {Number}  event.attendees[].a_attendee_id  Primary key ID of the atendee(user or group)
 * @param {Number}  event.attendees[].a_attendee_name  Name of the attendee(user or group)
 *
 * @param {Object}  event.tutors  The tutors already attached to this event
 * @param {Number}  event.tutors[].id  Primary key ID of the tutor from the `users` table
 * @param {String}  event.tutors[].title  Salutation (Mr. Dr. Mrs. Miss etc.)
 * @param {String}  event.tutors[].firstname  Tutor's preferred first name
 * @param {String}  event.tutors[].lastname  Tutor's prefrred last name
 * @param {Number}  event.tutors[].forgroup  The primary key of the group the tutor should be linked with to for this event
 *
 * @param {Object}  event.locations  The locations already attached to this event
 * @param {Number}  event.locations[].id Primary key ID of the location from the locations table
 * @param {String}  event.locations[].room  Room name
 * @param {String}  event.locations[].building  Building name
 * @param {Number}  event.locations[].forgroup  The primary key of the group this location should be linked with to for this event
 *
 * @returns {Object}
 */
export const buildgroupevents = (event) => {
    if (parseInt(event.isgroupteaching) === 1) {

        if (event.tutors == undefined) {
            event.tutors = {};
        }

        if (event.locations == undefined) {
            event.locations = {};
        }

        if (event.attendees == undefined) {
            event.attendees = {};
        }

        event.groupteachingevents = Object.values(event.attendees)
            .map((attendee) => {
                const tutor = Object.values(event.tutors)
                    .find((tutor) => attendee.a_id === tutor.forgroup);

                const location = Object.values(event.locations)
                    .find((location) => attendee.a_id === location.forgroup);

                let locationid, locationname;
                if (location == undefined) {
                    locationid = 'TBC';
                    locationname = 'TBC';
                } else {
                    locationid = location.id;
                    locationname = `${location.building} ${location.room}`;
                }

                let tutorid, tutorname;
                if (tutor == undefined) {
                    tutorid = undefined;
                    tutorname = undefined;
                } else {
                    const { id, title, firstname, lastname } = tutor;
                    tutorid = id;
                    tutorname = `${title} ${firstname} ${lastname}`;
                }
                return {
                    groupid: attendee.a_attendee_id,
                    groupname: attendee.a_name,
                    locationid,
                    locationname,
                    tutorid,
                    tutorname,
                };
            });

        event.groupteachingevents.sort((a, b) => {
            if (a.groupname === b.groupname) {
                return 0;
            } else {
                return (a.groupname < b.groupname) ? -1 : 1;
            }
        });
    } else {
        event.groupteachingevents = false;
        event.isgroupteaching = 0;
    }
    return event;
};
