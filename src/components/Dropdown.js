/**
 * Convert an array of objects into an array of <option> elements for insertion into a dropdown menu
 * @function dropdown
 * @memberof module:Components
 *
 * @param {Object[]}  options  An array of items to convert into a dropdown
 * @param {Number}  options[].id  The ID of the dropdown item.
 * @param {String}  options[].text  The text to display in the dropdown list
 * @param {Number[]}  [preSelected=[]]  An array of IDs (from the `options[].id` parameter) to preselect. By default, no options are preselected.
 *
 * @returns {HTMLOptionElement[]}  An array of <option> elemenents
 * @example
 * dropdown([{id: 21, text: 'hello world'}, {id: 22, text: 'Fubar'}], [21]);
 * //Will return:
 * [<option value="21" selected>hello world</option>, <option value="22">Fubar</option>]
 */
export function dropdown (options = [], preSelected = []) {
    return options.filter((option, array_index, options_array) => {
        return options_array.map((mapObj) => mapObj['id'])
            .indexOf(option['id']) === array_index;
    }).map(({ text, id }) => new Option(text, id, preSelected.includes(id)));
}
