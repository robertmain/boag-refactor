import { dropdown } from '../Dropdown';
const mount = require('jquery');

describe('Dropdown', () => {
    it('pre-selects specified records', () => {
        const option = mount(dropdown([
            {
                id: 2821,
                text: 'Hello world',
            },
        ], [2821]));

        expect(option.eq(0).attr('value')).toBe('2821');
        expect(option.eq(0).attr('selected')).toBeDefined();
    });

    it('lists all records provided', () => {
        const records = dropdown([
            {
                id: 2194,
                text: 'Example record',
            },
            {
                id: 1234,
                text: 'Another record',
            },
        ]);
        expect(records.length).toBe(2);
    });

    it('conatins no duplicate entries', () => {
        const options = mount(dropdown([
            {
                id: 16,
                text: 'First item',
            },
            {
                id: 16,
                text: 'Second item',
            },
        ]));
        expect(options.length).toBe(1);
    });

    it('contains no entries by default', () => {
        const options = mount(dropdown(undefined));

        expect(options.length).toBe(0);
    });
});
