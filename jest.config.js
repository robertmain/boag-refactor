module.exports = {
    'rootDir': './',
    'modulePaths': [
        '<rootDir>/src/',
    ],
    'collectCoverageFrom': [
        'src/**/*.{js}',
    ],
    'reporters': [
        'default',
        [
            'jest-junit', { 'outputDirectory': '<rootDir>/test/coverage' },
        ],
    ],
    'coverageDirectory': '<rootDir>/test/coverage',
    'collectCoverage': true,
    'verbose': true,
};
